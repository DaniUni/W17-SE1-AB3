package at.aau.ue3.bsp3;

public class PracticalExamChecker {
    public boolean isAdmissibleForExam(int age, boolean lessonsTaken, double pointsOnTheoryExam) {
        return age>=18 && lessonsTaken && pointsOnTheoryExam>=80;
    }
}
