package at.aau.ue3.bsp5;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	public Date nextDate(int d, int m, int y) {
		Calendar cal = Calendar.getInstance();
		cal.set(y, m, d);
        cal.add(Calendar.DATE, 1); //minus number would decrement the days
        return cal.getTime();
	}
}
