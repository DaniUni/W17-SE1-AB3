package at.aau.ue3.bsp1;

import java.math.BigDecimal;

/**
 *
 * @author
 * http://javahowto.blogspot.co.at/2011/08/example-of-expensive-computation.html
 *
 */
public class PiComputer implements java.io.Serializable {

    private static final long serialVersionUID = 227L;
    private static final BigDecimal FOUR  = BigDecimal.valueOf(4);
    private static final int roundingMode = BigDecimal.ROUND_HALF_EVEN;

   
    /**
     * Compute the value of pi to the specified number of digits after the
     * decimal point. The value is computed using Machin's formula: pi/4 =
     * 4*arctan(1/5) - arctan(1/239) and a power series expansion of arctan(x)
     * to sufficient precision.
     *
     * @param digits
     * @return
     */
    public static BigDecimal computePiWithMachin(int digits) {
        
        int scale = digits + 5;
        BigDecimal arctan1_5 = arctan(5, scale);
        BigDecimal arctan1_239 = arctan(239, scale);

        BigDecimal pi = arctan1_5.multiply(FOUR).subtract(arctan1_239).multiply(FOUR);
        
        return pi.setScale(digits, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Compute the value, in radians, of the arctangent of the inverse of the
     * supplied integer to the specified number of digits after the decimal
     * point. The value is computed using the power series expansion for the arc
     * tangent: arctan(x) = x - (x^3)/3 + (x^5)/5 - (x^7)/7 + (x^9)/9 ...
     *
     * @param inverseX
     * @param scale
     * @return
     */
    public static BigDecimal arctan(int inverseX, int scale) {
        
        BigDecimal result, number, term;
        BigDecimal invX  = BigDecimal.valueOf(inverseX);
        BigDecimal invX2 = BigDecimal.valueOf(inverseX * inverseX);
        
        number  = divide(scale, BigDecimal.ONE, invX, roundingMode);
        result = number;
        int i  = 1;
        
        do {
            number = divide(scale, number, invX2, roundingMode);
            int denom = 2 * i + 1;
            term = divide(scale, number, BigDecimal.valueOf(denom), roundingMode);
            if ((i % 2) != 0) {
                result = subtract(result, term);
            } else {
                result = add(result, term);
            }
            i++;
        } while (term.compareTo(BigDecimal.ZERO) != 0);
        return result;
    }

    private static BigDecimal add(BigDecimal result, BigDecimal term) {
        return result.add(term);
    }

    private static BigDecimal subtract(BigDecimal result, BigDecimal term) {
        return result.subtract(term);
    }

    private static BigDecimal divide(int scale, BigDecimal number, BigDecimal divisor, int roundingMode) {
        return number.divide(divisor, scale, roundingMode);
    }
}
