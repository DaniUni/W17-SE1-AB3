/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.aau.ue3.bsp1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;


public class Main {
    
    private static final int MAX_SCALE = 100000;
    
    private static NumberFormat formatter;
    
    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(25*1000); //wait until visualvm is started
        System.out.println("Calculation starting in 5s!");
        Thread.sleep(5*1000); //wait until visualvm is started
        System.out.println("Starting");

        // init the formatter
        formatter = new DecimalFormat("0.0E0");
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        formatter.setMinimumFractionDigits(15);
        
        // now compute PI
        List<BigDecimal> steps = new ArrayList<>();
        for (int scale = 1; scale <= MAX_SCALE; scale=scale+10) {
            BigDecimal bigDecimal = computePIWithScale(scale);
            steps.add(bigDecimal);
        }

        System.out.println(steps);

        computePIWithScale(5);
    }

    /**
     * compute PI with Scale, print the result and performance metrics
     * @param scale 
     */
    private static BigDecimal computePIWithScale(int scale) {
        BigDecimal result;
        long timestamp;
        
        timestamp = System.currentTimeMillis();
        result    = PiComputer.computePiWithMachin(scale);
        timestamp = System.currentTimeMillis() - timestamp;
        
        System.out.println("PI Computation ("+scale+") took " + timestamp);
        System.out.println("Pi is " + formatter.format(result));
        System.out.println("---\n" + result.toEngineeringString());

        return result;
    }

}
